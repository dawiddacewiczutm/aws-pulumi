# Install and use Pulumi

Download package from [pulumi](https://www.pulumi.com/docs/get-started/install/versions/) and add binaries to PATH

Configure cloud credentials provider (AWS in this case)

    aws configure

Create directory

    mkdir project-1 && cd project-1

Register [pulumi](https://app.pulumi.com/) account, next run pulumi command, you can accept defaults

    pulumi new python

Switch python shell to virtual env and install requirments

    source venv/bin/activate
    pip3 install -r requirements.txt
    pip3 install pulumi-aws

Configure aws region for pulumi

    pulumi config set aws:region us-east-1
    pulumi config set project-1:siteDir site


Make changes in \_\_main\_\_.py and run

    pulumi up

Create prod stack 

    pulumi stack init prod
    pulumi config set aws:region eu-west-1
    pulumi config set project-1:siteDir site

    pulumi stack ls

Destroy resources in stacks

    pulumi destroy
    pulumi stack select dev
    pulumi destroy

CI/CD (installed pulumi and configured aws credentials)

    cd project-1
    python3 -m venv venv
    source venv/bin/activate
    pip3 install -r requirements.txt
    pip3 install pulumi-aws
    pulumi up -y -s dev #Dev stack for example